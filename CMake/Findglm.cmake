#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
message(STATUS "glm")
#set(CMAKE_INCLUDE_PATH "${iMSTK_SRC}/build" ${CMAKE_INCLUDE_PATH})
#set(CMAKE_LIBRARY_PATH "${iMSTK_SRC}/build" ${CMAKE_LIBRARY_PATH})
#set(CMAKE_PREFIX_PATH "${iMSTK_SRC}/build" ${CMAKE_PREFIX_PATH})

find_path(glm_INCLUDE_DIR
  NAMES
    glm/glm.hpp
  HINTS
    ${iMSTK_LIB_EXTERNAL}/glm/src
    NO_DEFAULT_PATH
    #NO_CMAKE_ENVIRONMENT_PATH
    #NO_CMAKE_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    #CMAKE_FIND_ROOT_PATH_BOTH 
    #ONLY_CMAKE_FIND_ROOT_PATH 
    #NO_CMAKE_FIND_ROOT_PATH
)
mark_as_advanced(glm_INCLUDE_DIR)
message(STATUS "glm_INCLUDE_DIR : ${glm_INCLUDE_DIR}")

#-----------------------------------------------------------------------------
# Find package
#-----------------------------------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(glm
  REQUIRED_VARS
    glm_INCLUDE_DIR)

#-----------------------------------------------------------------------------
# If missing target, create it
#-----------------------------------------------------------------------------
if(GLM_FOUND AND NOT TARGET glm)
  add_library(glm INTERFACE IMPORTED)
  set_target_properties(glm PROPERTIES
    INTERFACE_LINK_LIBRARIES "${glm_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${glm_INCLUDE_DIR}"
  )
endif()
