#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
message(STATUS "imgui")

find_path(imgui_INCLUDE_DIR
  NAMES
    imgui.h
	HINTS
    ${iMSTK_LIB_EXTERNAL}/imgui/src
    NO_DEFAULT_PATH
    #NO_CMAKE_ENVIRONMENT_PATH
    #NO_CMAKE_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    #CMAKE_FIND_ROOT_PATH_BOTH 
    #ONLY_CMAKE_FIND_ROOT_PATH 
    #NO_CMAKE_FIND_ROOT_PATH
    )
mark_as_advanced(imgui_INCLUDE_DIR)
message(STATUS "imgui_INCLUDE_DIR : ${imgui_INCLUDE_DIR}")

#-----------------------------------------------------------------------------
# Find package
#-----------------------------------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(imgui
  REQUIRED_VARS
    imgui_INCLUDE_DIR)

#-----------------------------------------------------------------------------
# If missing target, create it
#-----------------------------------------------------------------------------
if(GLM_FOUND AND NOT TARGET imgui)
  add_library(imgui INTERFACE IMPORTED)
endif()
