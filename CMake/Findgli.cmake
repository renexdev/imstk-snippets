#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
find_path(gli_INCLUDE_DIR
  NAMES
    gli/gli.hpp
  HINTS
    ${iMSTK_LIB_EXTERNAL}/gli/src
    NO_DEFAULT_PATH
    #NO_CMAKE_ENVIRONMENT_PATH
    #NO_CMAKE_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    #CMAKE_FIND_ROOT_PATH_BOTH 
    #ONLY_CMAKE_FIND_ROOT_PATH 
    #NO_CMAKE_FIND_ROOT_PATH
    )
mark_as_advanced(gli_INCLUDE_DIR)
message(STATUS "gli_INCLUDE_DIR : ${gli_INCLUDE_DIR}")

#-----------------------------------------------------------------------------
# Find package
#-----------------------------------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gli
  REQUIRED_VARS
    gli_INCLUDE_DIR)

#-----------------------------------------------------------------------------
# If missing target, create it
#-----------------------------------------------------------------------------
if(GLM_FOUND AND NOT TARGET gli)
  add_library(gli INTERFACE IMPORTED)
  set_target_properties(gli PROPERTIES
    INTERFACE_LINK_LIBRARIES "${gli_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${gli_INCLUDE_DIR}"
  )
endif()
