#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
find_path(Assimp_INCLUDE_DIRS
  NAMES
    assimp/Importer.hpp
  HINTS
    ${iMSTK_LIB_EXTERNAL}/Assimp/src/include
    NO_DEFAULT_PATH
    #NO_CMAKE_ENVIRONMENT_PATH
    #NO_CMAKE_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    #CMAKE_FIND_ROOT_PATH_BOTH 
    #ONLY_CMAKE_FIND_ROOT_PATH 
    #NO_CMAKE_FIND_ROOT_PATH
    )
mark_as_advanced(Assimp_INCLUDE_DIRS)

#-----------------------------------------------------------------------------
# Find library
#-----------------------------------------------------------------------------
find_library(Assimp_LIBRARIES
  NAMES
    libassimp
    assimp
    libassimpd
    assimpd
    HINTS
      ${iMSTK_LIBS_PATH}
  )
mark_as_advanced(Assimp_LIBRARIES)

#-----------------------------------------------------------------------------
# Find package
#-----------------------------------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Assimp
  REQUIRED_VARS
    Assimp_INCLUDE_DIRS
    Assimp_LIBRARIES)

#-----------------------------------------------------------------------------
# If missing target, create it
#-----------------------------------------------------------------------------
if(Assimp_FOUND AND NOT TARGET Assimp)
  add_library(Assimp INTERFACE IMPORTED)
  set_target_properties(Assimp PROPERTIES
    INTERFACE_LINK_LIBRARIES "${Assimp_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${Assimp_INCLUDE_DIRS}"
  )
endif()
