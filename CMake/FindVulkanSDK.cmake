#-----------------------------------------------------------------------------
# Vulkan renderer
#-----------------------------------------------------------------------------

if(NOT DEFINED VULKAN_SDK OR NOT EXISTS ${VULKAN_SDK})
  set(VULKAN_SDK "$ENV{VULKAN_SDK}" CACHE PATH "Path to Vulkan SDK install directory." FORCE)
endif()
if(NOT EXISTS ${VULKAN_SDK})
  message(FATAL_ERROR "\nCan not support Vulkan renderer without Vulkan SDK.\nSet VULKAN_SDK to Vulkan SDK installation directory.\n\n")
endif()

#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
#find_path(VulkanSDK_INCLUDE_DIR
#  NAMES
#    vulkan/vulkan.h
#  PATHS
#    ${VULKAN_SDK}/include
#  )
find_path(VulkanSDK_INCLUDE_DIR
      NAMES vulkan/vulkan.h
      HINTS
      NO_DEFAULT_PATH
      "${VULKAN_SDK}/include"
      
      )
#find_path gives vulkan system headers
set(VulkanSDK_INCLUDE_DIR "${VULKAN_SDK}/include")
mark_as_advanced(VulkanSDK_INCLUDE_DIR)
message(STATUS "VulkanSDK_INCLUDE_DIR: ${VulkanSDK_INCLUDE_DIR}")
#-----------------------------------------------------------------------------
# Find library
#-----------------------------------------------------------------------------
find_library(VulkanSDK_LIBRARY
  NAMES
    vulkan
  HINTS
   "${VULKAN_SDK}/lib"
  )
mark_as_advanced(VulkanSDK_LIBRARY)

set(VulkanSDK_LIBRARIES ${VulkanSDK_LIBRARY})
message(STATUS "VulkanSDK_LIBRARY: ${VulkanSDK_LIBRARY}")
