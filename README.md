## iMSTK-snippets for Linux

***Important:*** this project is deprecated due to major modifications introduced in iMSTK 2.0. An alternative way 
to build just the examples without compiling all the projet is running the command make in the corresponding folder.

Requirements:
* iMSTK 1.0.0
* CMake 3.14 or later

## Build

Build iMSTK following the instructions in https://gitlab.kitware.com/iMSTK/iMSTK.
We use build instead of iMSTK-build as the project build directory.
Set the iMSTK source code path (not the build directory) using iMSTK_SRC variable in the CMakeLists.txt file or via the env vars. 
Define ${PROJECT_NAME}_USE_VK option option variable to select VTK or VK Renderer in each snippet. If you use VK you must set Vulkan SDK path in the VULKAN_SDK variable.
Then: 
* cd imstk-snippets
* mkdir build
* cd build
* cmake ..
* make -j$(nproc) 

Run the snippets from their build directory otherwise the assets files (.obj, shaders) won't be found.

